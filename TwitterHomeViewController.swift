//
//  TwitterHomeViewController.swift
//  TwitterUdemyApp
//
//  Created by Kerim Çağlar on 27/05/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

class TwitterHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
     
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tweetCell", for: indexPath) as! SingleTweetTableViewCell
        
        
        
        cell.tweet.isEditable = false
        
        let imageView = cell.profilePicture!
        imageView.layer.cornerRadius = 40
        
        
        return cell
    }

}
