//
//  HomeViewController.swift
//  TwitterUdemyApp
//
//  Created by Kerim Çağlar on 08/05/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Firebase

class HomeViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var mentionName: UITextField!
    
    @IBOutlet weak var fullName: UITextField!

    @IBOutlet weak var about: UITextField!
    
    var ref = FIRDatabase.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mentionName.delegate = self
        fullName.delegate = self
        about.delegate = self
        
    }

    

    @IBAction func startTwitter(_ sender: Any) {
        
        //Kullanıcının id'sini alıyoruz.
        let userId = FIRAuth.auth()?.currentUser?.uid
        print(userId!)
        
        ref.child("mentionName").child(self.mentionName.text!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if(!snapshot.exists())
            {
                self.ref.child("users").child(userId!).child("mentionName").setValue(self.mentionName.text!.lowercased())
                
                self.ref.child("users").child(userId!).child("fullName").setValue(self.fullName.text)
                
                self.ref.child("users").child(userId!).child("about").setValue(self.about.text)
                
                self.ref.child("mentionName").child(self.mentionName.text!.lowercased()).setValue(userId!)
                
            }
                
                //Burası kontrol edilmeli
            else
            {
                
                self.showAlert(title: "", message: "Bu kullanıcı adı kullanımda. Eşsiz bir kullanıcı adı belirlemeye çalışın.")
            }
            
            
            // ...
        })
        {
            (error) in
            print(error.localizedDescription)
        }
    }
    
    // Klavyede Done tuşu fonksiyonu
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
